Create easily distributable Python Projects
===========================================

# Table of Contents
1. [Example](#example)
2. [Example2](#example2)
3. [Third Example](#third-example)

## `close`(*par1*, *par2*)
Closes the designated component.

    close($c[ref])

 * par1: blabla
 * par2: biii




## Example
## Example2
## Third Example

1. [ Description. ](#desc)
2. [ Usage tips. ](#usage)

<a name="desc"></a>
## 1. Description

sometext

<a name="usage"></a>
## 2. Usage tips

sometext

iiIn order to create python projects that are easily distributable and installable, one needs to create several files (for example a setup.py) alongside with your actual python module. To make this process easy, there is cookiecutter. If you follow the instructions below, cookiecutter will create a skeleton folder that has the proper structure to get your python project started.

Dependencies
============

 * python
 * make (optional, useful for creating the documentation)

Using CookieCutter for your project
-----------------------------------

    $ pip install cookiecutter
    $ cookiecutter iaehttps://USERNAME@bitbucket.org/eiteneuer/cookiecutter-standard_python_project.git

You will be asked about your basic info.

 * Full Name
 * email
 * bitbucket username
 * project_name (This can include Whitespaces, like "Python Project")
 * app_name (This will determine the name of the folders, like "python_project)
 * project_short_description
 * version
 * which License?

This info will be used in the skeleton files and folders of your project. It includes and supports

 * Skeleton structure in line with the [Python Packacking Index User Guide](https://packaging.python.org/)
 * requirements.txt (for automatic installation of dependencies)
 * Sphinx documentation cababilities
 * testing capabilities
 * LICENSE file
 * README 
 * [wheel](https://wheel.readthedocs.io/en/stable/) creation


requirements.txt
----------------

One problem that arises when distributing python projects is the problem of dependencies. If you give someone your python scripts which included third party libraries, the other person can only use your scripts if they have them all installed themselves. To automate this process, we can simply put the required libraries inside _requirements.txt_ and have them automatically managed by the _setup.py_ script.

Sphinx autodocumentation
------------------------

A good documentation is key for any useful project. [Sphinx](http://www.sphinx-doc.org/en/master/index.html) can use the docstrings and comments inside the source code to create an html page from them. Sphinx is also configured to include your README and other documents.

How to exactly format the docs and comments to make them parsable to Sphinx is shown in an example python file `how_to_document_code.py` included in the project directory.

**Important**: while parsing Sphinx will import your python files. This means, that unless your code is nested inside in classes and functions definitions or protected by a `if __name__ == '__main__'` statement, those commands will get executed. If you want to use Sphinx, be careful with naked `os.remove(...)` statements in the code!


Sphinx usage
------------

    $ cd docs
    $ sphinx-apidoc -f -o . ../my_python_proj/
    $ make html
    $ firefox _build/html/index.html

Execute the `sphinx-apidoc` command, whenever you updated your sourcecode and you want to include the new version in the docs. This enables Sphinx to automatically include all classes/functions into the documentation from your package folder(s), which in this case is "my_python_proj"). For more information on this consult [sphinx-apidoc](http://www.sphinx-doc.org/en/master/man/sphinx-apidoc.html)

If you do not have make, you can replace the `make html` with 

    $ python -msphinx -M . _build


 

Wheels
------

[Wheels](https://wheel.readthedocs.io/en/stable/) allow you to distribute python packages easily. You can create them with

    $ python setup.py bdist_wheel

The wheel gets placed inside a newly created folder `dist`. It can be installed by someone else via

    $ pip install your_wheel.whl
   
Credits 
-------

Cookiecutter on github: [cookiecutter](https://github.com/audreyr/cookiecutter)

This project is a fork from: [wdm0006](https://github.com/wdm0006/cookiecutter-pipproject/)

Also used code from: [audreyr](https://github.com/audreyr/cookiecutter-pypackage)

