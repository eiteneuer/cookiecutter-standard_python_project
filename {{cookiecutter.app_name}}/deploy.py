from gevent.pywsgi import WSGIServer
from app import app
import sys

USAGE = '''python deploy.py <ip> <port>'''

if len(sys.argv) != 3:
    sys.exit(USAGE)
else:
    ip = sys.argv[1]
    port = int(sys.argv[2])


# ip = '0.0.0.0'
# port = 5000

http_server = WSGIServer((ip, port), app.server)
http_server.serve_forever()