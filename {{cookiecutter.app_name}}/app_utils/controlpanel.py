from dash import Dash
import dash_core_components as dcc
import dash_html_components as html

from dash.dependencies import Input, Output
import dash_ui as dui

'''
below we define dash elements used in the control panel:
    * Upload button (uploader)
    * Radio Button (radioitems)
    * Checklist (checklist)
    * and more

and various little text elements
'''

textinput = html.Div(
    dcc.Input(
        id='number-input',
        type='number',
        placeholder='Select Number'
    ),
    style={'padding-bottom':'10px'}
)

uploader = dcc.Upload(
    id='upload-data',
    children=html.Div([
        'Drag and Drop or ',
        html.A('Select Files')
    ]),
    style={
        # 'width': '200px',
        # 'height': '50px',
        # 'lineHeight': '60px',
        # 'borderWidth': '1px',
        'borderStyle': 'dashed',
        'borderRadius': '5px',
        'textAlign': 'center',
        'margin': '10px',
    },
    multiple=True # Allow multiple files to be uploaded
)

showing_filenames_uploaded_data = html.Pre(
    id='info-data',
    children = 'try out the Drag and Drop Element above me!',
    style = {
        'border': 'thin lightgrey solid',
        'overflowX': 'scroll',
        'overflowY': 'scroll',
        'height': '200px'
    })

radioitems = dcc.RadioItems(
    id = 'radio-ltm-optical-axis',
    options=[
        {'label': 'LTM', 'value': 'LTM'},
        {'label': 'Optical Axis', 'value': 'Optical Axis'},
        {'label': 'Label123', 'value': 'xyz'}
    ],
    value = 'None',
    #style = #"width": "300px", "padding-top": "10px"}
)

text = html.Div('Nice Text', style = {'padding-top': '10px'})

checklist = dcc.Checklist(
    id = 'checklist',
    options=[
        {'label': 'Show both Caustics', 'value': 'Both'},
        {'label': 'abcdefg', 'value': 'a'}
    ],
    value=['Both'],
    style = {"padding-top": "10px"}
)

dropdown = dcc.Dropdown(
    id="dropdown",
    options=[
        {'label': "example", 'value': "show"},
        {'label': 'dropdown_choice_1', 'value': 'ddc1'},
        {'label': 'dropdown_choice_2', 'value': 'ddc2'},
    ],
    value="show"
)

button_and_download = html.Div(className='column',
    children=[
        html.Button('create table report', id='button-create-table'),
        html.A(
            children= html.Button('Download example file', id='download-button'), 
            href='/download_example/',
            id='download-link',
            style={'padding-left':'10px'},
            target='_blank',
            rel='noreferrer noopener'            
        ),
        #html.Button('create current report2', id='button-single-report2'),
        #dcc.Loading(id="loading-1", children=[html.Div(id="output-1", style={'width':'5px'})], type="default", style={'width':'20px'}),
    ],
    id = 'report-current-single',
    style = {'padding-bottom':'20px'}
)

slider = html.Div(

    dcc.Slider(
        id='simple-slider',
        min=0,
        max=5,
        marks={i: 'Label\u00a0{}'.format(i) for i in range(6)},
        value=5,
    ),

    style = {'padding-left': '15px', 'padding-right': '15px', 'padding-bottom': '30px'}
)  

ranged_slider = html.Div(

    dcc.RangeSlider(
        marks={i: 'Label\u00a0{}'.format(i) for i in range(-2, 4)},
        min=-2,
        max=3,
        value=[-1, 0]
    ),

    style = {'padding-left': '15px', 'padding-right': '15px'}
)  


def get_controlpanel(session_id):
    '''
    Create the controlpanel visible on the left. Gets organised via collapsable sections.
    In these Sections, you can create groups. 
    
    All actual dash elements (buttons, dropdowns, tables etc)
    which are visible to the user get added to the groups.

    These groups get then added to the sections.
    '''

    controlpanel = dui.ControlPanel(_id="controlpanel")

    #################
    ## SECTION ONE ##
    #################
    controlpanel.create_section(
        section="section_1",
        section_title="Section 1",
        defaultOpen=True
    )

    ###########################
    ## SECTION ONE / GROUP 1 ##
    ###########################
    controlpanel.create_group(
        group="download_group",
        group_title="Load in new Data"
    )
    
    controlpanel.add_element(uploader, "download_group") # add our uploader to the subgroup
    controlpanel.add_element(showing_filenames_uploaded_data, "download_group") # show info about downloaded files


    #################
    ## SECTION TWO ##
    #################
    controlpanel.create_section(
        section="section_2",
        section_title="Section 2",
        defaultOpen=True
    )

    ###########################
    ## SECTION TWO / GROUP 1 ##
    ###########################
    controlpanel.create_group(
        group="dash_elements",
        group_title="Section Two / Group 1",
    )


    controlpanel.add_element(radioitems, "dash_elements")
    controlpanel.add_element(text, "dash_elements")
    controlpanel.add_element(checklist, "dash_elements")


    ###########################
    ## SECTION TWO / GROUP 2 ##
    ###########################
    controlpanel.create_group(
        group="more_dash_elements",
        group_title="Section Two / Group 2",
    )

    controlpanel.add_element(button_and_download, "more_dash_elements")
    controlpanel.add_element(dropdown, "more_dash_elements")


    ###########################
    ## SECTION TWO / GROUP 3 ##
    ###########################
    controlpanel.create_group(
        group="even_more_dash_elements",
        group_title="Section Two / Group 3",
    )

    controlpanel.add_element(textinput, "even_more_dash_elements")
    controlpanel.add_element(slider, "even_more_dash_elements")
    controlpanel.add_element(ranged_slider, "even_more_dash_elements")

   

    ##############################
    # add groups to all sections #
    ##############################

    controlpanel.add_groups_to_section("section_1", ["download_group"])
    controlpanel.add_groups_to_section("section_2", ["dash_elements", "more_dash_elements", "even_more_dash_elements"])

    return controlpanel
