import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html


def get_example_table():
    # import pandas as pd
    # df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/solar.csv')
    
    
    table = dash_table.DataTable(
        id='data-table',
        # columns=[{"name": i, "id": i} for i in df.columns],
        # data=df.to_dict('records'),
        style_cell = {'fontSize': 20},
        style_table={'overflowX': 'scroll'},

    )

    return html.Div(
        children = table,
        style = {'padding-right': '5px'}
    )

    return table