# GUI LIBRARIES
import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.graph_objects as go

# OTHER EXTERNAL LIBRARIES
import numpy as np
import base64 # used in parse_data, decipher incoming data uploaded by user
import flask # used for downloading files

import logging
log = logging.getLogger("app.callbacks")

def parse_data(content):
    '''
    Utility for the Upload Data Element.
    '''
    content_type, content_string = content.split(',')
    decoded = base64.b64decode(content_string).decode("utf-8", "backslashreplace")

    return decoded

def register(app):
    '''
    If "contents" of upload-data change, update children property of the
    dash element with the id "info-data". We can additionally retrieve info
    about the session-id (unique to each user) and more info about the uploaded data.
    '''

    @app.callback(
                [Output('info-data', 'children')],
                
                [Input('upload-data', 'contents')],
                
                [State('upload-data', 'filename'),
                 State('session-id', 'children')]
    )
    def upload_data(list_of_contents, list_of_names, session_id): # function has 3 inputs: 1 Input and 2 States, in the order specified above
        
        print('in upload data, session_id', session_id)
        print('list of contents (first 200 chars)', str(list_of_contents)[:200])
        print('list_of_names', list_of_names)
        data = app.data[session_id]

        if list_of_contents is not None:

            message = f'Received {len(list_of_contents)} files.\n\nFilenames:\n\n\t' + '\n\t'.join(list_of_names)

            contents = [parse_data(content) for content in list_of_contents]
            
            for filename, content in zip(list_of_names, contents):
            
                message += f'\n\n{35*"-"}\n\ncontents of {filename}:\n\n{content}'
            
                # IMPORTANT. SAVE THE DATA FOR FUTURE CALLBACKS.
                # THERE ARE MANY WAYS TO **SAVELY** STORE DATA FOR DIFFERENT USERS.
                # THIS IS A SIMPLE WAY TO DO IT
                data[filename] = content

            return [message]
    
        else:
            
            return ['try out the Drag and Drop Element above me!']



    @app.callback(
            [Output('data-table', 'columns'),
             Output('data-table', 'data')],
            
            [Input('button-create-table', 'n_clicks')],

            [State('session-id', 'children')]
    )
    def update_table(n_clicks, session_id):
        
        print('\nupdating table...')
        if (n_clicks is not None) and (session_id is not None):
            print('yippie', n_clicks, session_id)

            data = app.data[session_id]
            
            data['clicks_updating_table_button'] = n_clicks

            if app is not None:
                print(app.data)
            else:
                print('app is None')
            #print(f'keys of app_data:', app_data.keys())
            # try:
            #     print('bla')
                
            #     #print(f'({session_id}), app.data=', app.data)
            # except Exception as e:
            #     print(e)

            #print('_DATA', _DATA)

            cols = ['spalte 1', 'spalte 2', 'drei']
           
            data = [{col: col} for col in cols]

            data.extend([{'spalte 1': n_clicks}, {'spalte 2' : session_id[:20]}])

            columns = [{'name':col, 'id': col} for col in cols]

            return [columns, data]
        
        else:

            print('nothing happened yet...', n_clicks, session_id)
            return [dash.no_update, dash.no_update]



    @app.callback([Output('download-link', 'href')], 
                  [Input('download-button', 'n_clicks')],
                  [State('session-id', 'children')])
    def update_link(n_clicks, session_id):
        url = f'/download_example?n_clicks={n_clicks}&session_id={session_id}'
        print(f'in updating link ... {url}')
        return [url]

    @app.server.route('/download_example')
    def download_excel():
        '''
        This Route gets called everytime the Button with id 'download-button' is pressed.
        Clicking the Button changes the download link of the href and hence we go into this
        function every time. The reason is, flask evaluates the routes only once!
        If we did not change the link, we can only offer static files as downloads, not dynamically created ones.

        Please see https://community.plot.ly/t/allowing-users-to-download-csv-on-click/5550/8 for a discussion.
        '''
        
        n_clicks = flask.request.args.get('n_clicks')
        session_id = flask.request.args.get('session_id')

        print(f'download_excel starting ... n_clicks={n_clicks}, session_id={session_id}')
    
        with open('file.txt','w') as file:
            testline = f'ho-ho-ho.{n_clicks}..\n\nurl:\n/dash/urlToDownload?n_clicks={n_clicks}&session_id={session_id}'
            file.writelines(testline)

        strIO = open('file.txt','rb')
        strIO.seek(0)

        return flask.send_file(strIO,
            attachment_filename='file.txt',
            as_attachment=True)














































    '''
    example callback ...
    @app.callback(
        [Output('simple-slider', 'value')],
        [Input('number-input', 'value')],
        [State('session-id', 'children')]
    )
    def change_slider(value, session_id):

        return [value]
    '''