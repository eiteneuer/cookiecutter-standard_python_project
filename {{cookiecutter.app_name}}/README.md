{{ cookiecutter.project_name }}
==========================================================

version number: {{ cookiecutter.version }}
author: {{ cookiecutter.full_name }}

Overview
--------

{{ cookiecutter.project_short_description }}


Development
-----------

Execute

    $ python app.py

Deployment
----------

Execute

    $ python start_server <ip> <port>

to start the deployment server. Example to host on your own pc:

    $ python start_server 0.0.0.0 8000

Others can use your app if they type in

    192.168.1.xxx:8000

in their Browser. Type in `ipconfig` to find out your IPv4-Adress in the Aconity3D network and replace `xxx` accordingly.


Contributing
------------

Why not be the first?

Example
-------

TBD
