# -*- coding: utf-8 -*-

"""Main module."""

class bing:
    '''
    This is a testclass
    '''

    def __init__(self, name, var):
        '''
        creates some vars
        
        :type name: string
        :param name: some nice name

        :type var: float
        :param var: store a number
        
        '''
        self.name = name
        self.var = var

    def printer(self):
        '''
        some printing func
        '''
        print(self.var, self.name)


if __name__ == '__main__':
    binger = bing("eins risiko", "zwei")
    binger.printer()
