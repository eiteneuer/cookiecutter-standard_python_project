.. highlight:: shell

============
Installation
============


From sources
--------------

There are two ways to install {{ cookiecutter.project_name }}. If you have the wheel, enter

.. code-block:: console

    $ pip install {{ cookiecutter.app_name }}_insert_exact_wheelname_here.whl

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From Bitbucket
---------------

The sources for {{ cookiecutter.project_name }} can be downloaded from the `Bitbucket repo`_.

Clone the public repository:

.. code-block:: console

    $ git clone git://bitbucket.org:{{ cookiecutter.bitbucket_username }}/{{ cookiecutter.app_name }}.git


Then, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Bitbucket repo: https://bitbucket.com/{{ cookiecutter.bitbucket_username }}/{{ cookiecutter.app_name }}
.. _tarball: https://bitbucket.com/{{ cookiecutter.bitbucket_username }}/{{ cookiecutter.app_name }}/tarball/master
