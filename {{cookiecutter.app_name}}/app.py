# dash
from dash import Dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

# nice user interface for dash
import dash_ui as dui

import pandas as pd
import plotly.graph_objs as go

# for generating the session id, each user of the app receives a unique id
import uuid
import time

# user defined libraries
from app_utils import elements
from app_utils import callbacks
from app_utils import controlpanel


###########
# LOGGING #
###########
import logging

logger = logging.getLogger("app")
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler('app.log')

fh.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s %(levelname)s - %(message)s')
fh.setFormatter(formatter)

logger.addHandler(fh)
logger.info("starting app...")

#######
# APP #
#######


app = Dash(__name__)

#app.config['suppress_callback_exceptions'] = True # Activate if there are some dash components that only get created in callbacks and not in the initial layout
app.title = "{{cookiecutter.app_name}}" # stores session data for all users

app.data = {}

grid = dui.Grid(
    _id="grid",
    num_rows=12, # maximum num_rows: 12
    num_cols=12, # maximum num_cols: 12
    grid_padding=5
)

# change according to your need (col + row indexes start at 1)

grid.add_graph(col=1, row=1, width=4, height=12, graph_id="VERTICAL_GRAPH") # these ids get used in the callbacks later on to create interactivity

grid.add_graph(col=5, row=1, width=8, height=6, graph_id="HORIZONTAL_GRAPH")

grid.add_graph(col=5, row=7, width=4, height=6, graph_id="SMALL_LEFT")

#grid.add_graph(col=9, row=7, width=4, height=6, graph_id="SAMLL_RIGHT")

table = elements.get_example_table()
grid.add_element(element=table, col=9, row=7, width=4, height=6)

logo = html.Div(html.Img(src=app.get_asset_url('aconity_logo.png')))



# generates layout, should not be changed
def serve_layout(app): # little wrapper function
    
    def _serve_layout(): # actual layout function

        session_id = str(uuid.uuid4())
        now = time.time()

        app.data[session_id] = {}
        app.data[session_id]['__timestamp'] = now

        print(f'\n------------------------------------------\nnew session id requested: {session_id}')
        print(f'app.data at startup: {app.data}\n------------------------------------------\n')

        #clean old entries
        day = 24 * 60 * 60 # seconds 1 day
        
        for sid in app.data.keys():
            if now - app.data[sid]['__timestamp'] > 7 * day:
                print(f'deleting session_id {sid} as it is too old')
                del app.data[sid]

        session = html.Div(str(session_id), id='session-id', style={'display': 'none'}) # hidden state

        return html.Div(
            
            [
                session,
                dui.Layout(
                    grid=grid,
                    controlpanel=controlpanel.get_controlpanel(session_id),
                    logo=logo
                )
            ],
            style={
                'height': '100vh',
                'width': '100vw',
            }
        )

    return _serve_layout #return layout function
    

app.layout = serve_layout(app) # defines layout of app

callbacks.register(app) # create callbacks


if __name__ == "__main__":
    app.run_server(debug=True, host='0.0.0.0', port=8089)
